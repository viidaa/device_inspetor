package com.kaixindev.deviceinspetor;

import java.util.List;

import android.app.Activity;
import android.content.res.Resources;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.widget.TextView;

public class DeviceMainActivity extends Activity {

	private TextView mKeyView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Resources res = getResources();
		DisplayMetrics display = res.getDisplayMetrics();
		
		TextView densityView = (TextView)findViewById(R.id.screen_density);
		densityView.setText(res.getString(R.string.screen_density, display.density));
		
		TextView dpiView = (TextView)findViewById(R.id.screen_dpi);
		dpiView.setText(res.getString(R.string.screen_dpi, display.densityDpi, ScreenUtil.getNameOfDPI(display.densityDpi)));
		
		TextView resoPxView = (TextView)findViewById(R.id.screen_resolution_px);
		resoPxView.setText(res.getString(R.string.screen_resolution_px, display.widthPixels, display.heightPixels));
		
		TextView resoDpView = (TextView)findViewById(R.id.screen_resolution_dp);
		resoDpView.setText(res.getString(R.string.screen_resolution_dp, display.widthPixels / display.density, display.heightPixels / display.density));
		
		TextView fontsView = (TextView)findViewById(R.id.fonts_scaling_factor);
		fontsView.setText(res.getString(R.string.fonts_scaling_factor, display.scaledDensity));
		
		mKeyView = (TextView)findViewById(R.id.key);
		
		detectCamera();
	}
	
	private void detectCamera() {
		try {
            Camera mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
            Camera.Parameters params = mCamera.getParameters();
            
            String info = "camera:\n";
            info += "supported preview formats:";
            List<Integer> preFormats = params.getSupportedPreviewFormats();
            for (Integer format : preFormats) {
            	info += format + ",";
            }
            info += "\n";
            
            List<Camera.Size> preSizes = params.getSupportedPreviewSizes();
            info += "supported preview sizes:";
            for (Camera.Size size : preSizes) {
            	info += size.width + "x" + size.height + " ";
            }
            info += "\n";
            
            TextView cameraInfoView = (TextView)findViewById(R.id.camera_info);
            cameraInfoView.setText(info);
            
            mCamera.release();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		mKeyView.setText(event.toString());
		return false;
	}
}






