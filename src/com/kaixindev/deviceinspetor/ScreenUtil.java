package com.kaixindev.deviceinspetor;

public class ScreenUtil {
	public static String getNameOfDPI(int dpi) {
		switch (dpi) {
		case 120:
			return "ldpi";
		case 160:
			return "mdpi";
		case 240:
			return "hdpi";
		case 320:
			return "xhdpi";
		case 480:
			return "xxhdpi";
		default:
			return "not specified";
		}
	}
}
